ARG UBUNTU_VER=20.04
FROM --platform=amd64 ubuntu:$UBUNTU_VER

ARG IMG_VER=0.1
ARG REPO_TAG=0.1
ARG CONAN_VER=1.30.1
ARG PYTHON_VER=3.8

LABEL toolchains.core-image.vendor="InSAT LTD"
LABEL toolchains.core-image.version=$IMG_VER
LABEL toolchains.core-image.description="Base image for create target toolchain."
LABEL toolchains.core-image.maintainer="szhuravlev@insat.ru"
LABEL toolchains.core-image.repository.url=https://gitlab.com/insatru/docker/toolchains/core-image.git
LABEL toolchains.core-image.repository.tag=$REPO_TAG
LABEL toolchains.core-image.ubuntu.arch=amd64
LABEL toolchains.core-image.ubuntu.version=$UBUNTU_VER
LABEL toolchains.core-image.conan.version=$CONAN_VER
LABEL toolchains.core-image.python.version=$PYTHON_VER


ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update
RUN apt-get -y --no-install-recommends --no-cache install cmake make python$PYTHON_VER python3-pip wget curl
RUN pip3 install conan==$CONAN_VER
